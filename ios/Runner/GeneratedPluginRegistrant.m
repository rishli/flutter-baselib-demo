//
//  Generated file. Do not edit.
//

// clang-format off

#import "GeneratedPluginRegistrant.h"

#if __has_include(<connectivity_plus/FLTConnectivityPlusPlugin.h>)
#import <connectivity_plus/FLTConnectivityPlusPlugin.h>
#else
@import connectivity_plus;
#endif

#if __has_include(<device_info_plus/FLTDeviceInfoPlusPlugin.h>)
#import <device_info_plus/FLTDeviceInfoPlusPlugin.h>
#else
@import device_info_plus;
#endif

#if __has_include(<flutter_baselib/FlutterBaselibPlugin.h>)
#import <flutter_baselib/FlutterBaselibPlugin.h>
#else
@import flutter_baselib;
#endif

#if __has_include(<flutter_bugly_plugin/FlutterBuglyPlugin.h>)
#import <flutter_bugly_plugin/FlutterBuglyPlugin.h>
#else
@import flutter_bugly_plugin;
#endif

#if __has_include(<flutter_nativedialog_plugin/FlutterNativedialogPlugin.h>)
#import <flutter_nativedialog_plugin/FlutterNativedialogPlugin.h>
#else
@import flutter_nativedialog_plugin;
#endif

#if __has_include(<flutter_xlog_plugin/FlutterXlogPlugin.h>)
#import <flutter_xlog_plugin/FlutterXlogPlugin.h>
#else
@import flutter_xlog_plugin;
#endif

#if __has_include(<fluttertoast/FluttertoastPlugin.h>)
#import <fluttertoast/FluttertoastPlugin.h>
#else
@import fluttertoast;
#endif

#if __has_include(<package_info_plus/FLTPackageInfoPlusPlugin.h>)
#import <package_info_plus/FLTPackageInfoPlusPlugin.h>
#else
@import package_info_plus;
#endif

#if __has_include(<path_provider/FLTPathProviderPlugin.h>)
#import <path_provider/FLTPathProviderPlugin.h>
#else
@import path_provider;
#endif

#if __has_include(<permission_handler/PermissionHandlerPlugin.h>)
#import <permission_handler/PermissionHandlerPlugin.h>
#else
@import permission_handler;
#endif

#if __has_include(<shared_preferences/FLTSharedPreferencesPlugin.h>)
#import <shared_preferences/FLTSharedPreferencesPlugin.h>
#else
@import shared_preferences;
#endif

#if __has_include(<sqflite/SqflitePlugin.h>)
#import <sqflite/SqflitePlugin.h>
#else
@import sqflite;
#endif

#if __has_include(<webview_flutter/FLTWebViewFlutterPlugin.h>)
#import <webview_flutter/FLTWebViewFlutterPlugin.h>
#else
@import webview_flutter;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [FLTConnectivityPlusPlugin registerWithRegistrar:[registry registrarForPlugin:@"FLTConnectivityPlusPlugin"]];
  [FLTDeviceInfoPlusPlugin registerWithRegistrar:[registry registrarForPlugin:@"FLTDeviceInfoPlusPlugin"]];
  [FlutterBaselibPlugin registerWithRegistrar:[registry registrarForPlugin:@"FlutterBaselibPlugin"]];
  [FlutterBuglyPlugin registerWithRegistrar:[registry registrarForPlugin:@"FlutterBuglyPlugin"]];
  [FlutterNativedialogPlugin registerWithRegistrar:[registry registrarForPlugin:@"FlutterNativedialogPlugin"]];
  [FlutterXlogPlugin registerWithRegistrar:[registry registrarForPlugin:@"FlutterXlogPlugin"]];
  [FluttertoastPlugin registerWithRegistrar:[registry registrarForPlugin:@"FluttertoastPlugin"]];
  [FLTPackageInfoPlusPlugin registerWithRegistrar:[registry registrarForPlugin:@"FLTPackageInfoPlusPlugin"]];
  [FLTPathProviderPlugin registerWithRegistrar:[registry registrarForPlugin:@"FLTPathProviderPlugin"]];
  [PermissionHandlerPlugin registerWithRegistrar:[registry registrarForPlugin:@"PermissionHandlerPlugin"]];
  [FLTSharedPreferencesPlugin registerWithRegistrar:[registry registrarForPlugin:@"FLTSharedPreferencesPlugin"]];
  [SqflitePlugin registerWithRegistrar:[registry registrarForPlugin:@"SqflitePlugin"]];
  [FLTWebViewFlutterPlugin registerWithRegistrar:[registry registrarForPlugin:@"FLTWebViewFlutterPlugin"]];
}

@end
