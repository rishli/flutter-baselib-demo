import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/common/router/navigator_utils.dart';
import 'package:baselib_demo/common/util/env/dev_env_utils.dart';
import 'package:baselib_demo/common/util/ui_utils.dart';

///@date:  2021/4/21 13:56
///@author:  lixu
///@description: debug页面使用
class DebugViewModel extends BaseCommonViewModel {
  String _screenInfo = '';
  String _devEnvInfo = '';
  String _appInfo = '';
  String _deviceInfo = '';
  bool _isShowSwitchServerEnv = false;

  @override
  String getTag() {
    return 'DebugViewModel';
  }

  ///加载数据
  @override
  Future onLoading(BuildContext context) async {
    _screenInfo = UIUtils.screenInfoDesc;

    _isShowSwitchServerEnv = await DevEnvUtils.isShowSwitchServerEnvUI();
    _devEnvInfo = await DevEnvUtils.getDevEnvInfoDesc();

    _appInfo = '应用名称：${await PackageUtils.getAppName()}\n'
        '应用包名：${await PackageUtils.getPackageName()}\n'
        '版本号：${await PackageUtils.getVersionName()}\n'
        '版本code：${await PackageUtils.getBuildNumber()}\n';

    _deviceInfo = '手机品牌：${await DeviceUtils.getBrand()}\n'
        '手机型号：${await DeviceUtils.getModel()}\n'
        '系统：${DeviceUtils.getOsType()}\n'
        '系统版本：${await DeviceUtils.getOsVersion()}\n';

    ///TODO 添加其它需要显示的debug信息
  }

  String get devEnvInfo => _devEnvInfo;

  String get appInfo => _appInfo;

  String get deviceInfo => _deviceInfo;

  bool get isShowSwitchServerEnv => _isShowSwitchServerEnv;

  String get screenInfo => _screenInfo;

  @override
  bool isSuccess() {
    return _screenInfo.isNotEmpty;
  }

  ///修改服务器环境
  Future<void> onChangeSwitchServerEnv(BuildContext context) async {
    bool isRelease = !await DevEnvUtils.getIsReleaseServerEnv();
    bool isSuccess = await DevEnvUtils.setServerEnv(isRelease);
    if (isSuccess) {
      Future.delayed(Duration(seconds: 2), () {
        NavigatorUtils.exitApp();
      });
    }
  }
}
