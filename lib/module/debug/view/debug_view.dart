import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/module/debug/view_model/debug_viewmodel.dart';

import 'sys_info_view.dart';

///@date:  2021/4/21 12:10
///@author:  lixu
///@description:  调试页，显示设备，应用相关信息
class DebugView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('调试页'),
      ),
      body: ChangeNotifierProvider(
        create: (_) {
          return DebugViewModel();
        },
        builder: (BuildContext context, Widget? child) {
          return BaseView<DebugViewModel>(
            child: SysInfoView(),
          );
        },
      ),
    );
  }
}
