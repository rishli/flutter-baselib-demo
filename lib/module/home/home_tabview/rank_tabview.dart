import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/5/18 11:31
///@author:  lixu
///@description: app主界面，底部首页tab包含的顶部：排行tab
class RankTabView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    LogUtils.i("RankTabView", "build");
    return Container(
      child: Center(
        child: Text('排行'),
      ),
    );
  }
}
