import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/module/login/view_model/logout_view_model.dart';

import 'common/config/app_config.dart';
import 'common/manager/sdk_manager.dart';
import 'common/router/routes.dart';
import 'common/util/one_context_utils.dart';
import 'common/view_model/userinfo_view_model.dart';

///@date:  2021/4/20 17:08
///@author:  lixu
///@description: 应用入口类
class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  void initState() {
    super.initState();

    ///初始化三方库
    SdkManager.init(context);
  }

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      ///设置android状态栏为透明的沉浸。写在组件渲染之后，是为了在渲染后进行set赋值，覆盖状态栏，写在渲染之前MaterialApp组件会覆盖掉这个值。
      SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(statusBarColor: Colors.transparent);
      SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
    }

    return SdkManager.initScreenUtil(() => MultiProvider(
          providers: [
            ChangeNotifierProvider.value(value: LogoutViewModel()),
            ChangeNotifierProvider.value(value: UserInfoViewModel()),
          ],
          child: MaterialApp(
            title: AppConfig.appName,
            onGenerateRoute: Routes.router.generator,
            //初始化全局context
            builder: OneContextUtils.oneContextBuilder,
            navigatorKey: OneContextUtils.oneContextKey,
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
          ),
        ));
  }

  @override
  void dispose() {
    super.dispose();
    SdkManager.dispose();
  }
}
