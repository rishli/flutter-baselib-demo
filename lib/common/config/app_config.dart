///@date:  2021/4/20 17:54
///@author:  lixu
///@description: 应用全局配置类
class AppConfig {
  AppConfig._();

  static const appName = 'T游';

  static const initPageIndex = 0;
  static const pageSize = 20;

  ///APP保存在外部存储根目录
  static const appRootDir = '/storage/emulated/0/TGameApp/';

  ///日志存储目录
  static const logCacheDir = '/xlog/';

  ///log 默认目录
  static const defaultLogCacheDir = '$appRootDir$logCacheDir';
}
