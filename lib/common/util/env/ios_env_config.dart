///@date:  2021/4/27 15:40
///@author:  lixu
///@description: IOS环境配置
///对应的Android相关的配置参数，在工程目录/android/config.gradle文件中
class IosEnvConfig {
  IosEnvConfig._();

  ///控制台是否打印日志
  static const bool isConsoleLog = true;

  ///是否使用正式服务器
  static const bool isReleaseServer = false;
}
