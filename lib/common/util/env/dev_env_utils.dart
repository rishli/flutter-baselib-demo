import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/common/manager/sdk_manager.dart';

import 'env_config_plugin.dart';

///@date:  2021/4/21 09:33
///@author:  lixu
///@description: 开发环境工具方法
class DevEnvUtils {
  ///app当前使用的服务器环境保存在本地的key
  static const _serverUseEnvKey = '_serverUseEnvKey';

  ///当前是否使用的正式服
  ///仅当_isReleaseServerEnv=false时，才使用该字段
  static bool? _isCurrentUseReleaseEvn;

  ///是否已经加载过本地保存的[_serverUseEnvKey]标记
  static bool _isLoadedLocalCache = false;

  DevEnvUtils._();

  ///判断是否是debug（运行调试）模式
  ///当前方法与当前使用的服务器是正式服还是测试服没有关联
  static bool isDebug() {
    bool inDebugMode = false;
    assert(inDebugMode = true);
    return inDebugMode;
  }

  ///是否是正式服务器环境
  ///用于标记app当前使用的服务器环境
  ///与是否是debug模式没有关联
  static Future<bool> _isReleaseServerEnv() async {
    return await EnvConfigPlugin.isReleaseServer();
  }

  ///是否显示切换服务器环境的UI
  ///什么情况下显示切换环境的按钮？
  ///isReleaseServerEnv=false时
  static Future<bool> isShowSwitchServerEnvUI() async {
    return !await _isReleaseServerEnv();
  }

  ///获取当前是否是正式服环境
  ///调用接口时根据当前方法判断服务器环境，从而选择不同的baseUrl
  ///[return] true：使用正式服，false：使用测试服
  static Future<bool> getIsReleaseServerEnv() async {
    if (await _isReleaseServerEnv()) {
      return true;
    } else {
      if (_isCurrentUseReleaseEvn == null && !_isLoadedLocalCache) {
        ///获取本地保存的标记
        _isCurrentUseReleaseEvn = await SpUtils.getBool(_serverUseEnvKey);
        _isLoadedLocalCache = true;
      }

      return _isCurrentUseReleaseEvn != null && _isCurrentUseReleaseEvn!;
    }
  }

  ///设置app使用的服务器环境（用于测试阶段在app内手动切换）
  ///仅isShowSwitchServerEnvUI=true时，才会调用该方法
  ///[isRelease]是否是正式服
  static Future<bool> setServerEnv(bool isRelease) async {
    bool isSuccess = await SpUtils.setBool(_serverUseEnvKey, isRelease);
    if (isSuccess) {
      ToastUtils.showDebug('服务器环境已经切换为：${isRelease ? '正式服' : '测试服'}\n请重启应用', isShowLong: true);
      _isCurrentUseReleaseEvn = isRelease;
    }
    return isSuccess;
  }

  ///获取开发环境信息描述
  static Future<String> getDevEnvInfoDesc() async {
    String desc = '当前使用的服务器环境：${await DevEnvUtils.getIsReleaseServerEnv() ? '正式服' : '测试服'}\n'
        '是否显示切换服务器的按钮：${await isShowSwitchServerEnvUI()}\n'
        '控制台是否打印日志：${await SdkManager.isConsolePrintLog()}\n'
        '当前代码是否混淆：${await EnvConfigPlugin.isProguard()}\n'
        '当前是否是debug模式：${isDebug()}\n';
    return desc;
  }
}
