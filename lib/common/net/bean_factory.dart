
import 'package:baselib_demo/module/login/bean/login_result_bean.dart';

///@date:  2021/2/26 14:02
///@author:  lixu
///@description: http json解析bean
class BeanFactory {
  ///json解析
  ///[jsonData] json map
  static T? parseObject<T>(Map<String, dynamic>? jsonData) {
    if (jsonData == null) {
      return null;
    } else if (T.toString() == "LoginResultBean") {
      return LoginResultBean.fromJsonMap(jsonData) as T;
    } else {
      ///TODO 添加其它待解析的Bean对象
      return jsonData as T;
    }
  }
}
