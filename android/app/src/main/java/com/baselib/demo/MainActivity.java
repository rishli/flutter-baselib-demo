package com.baselib.demo;

import androidx.annotation.NonNull;

import com.baselib.demo.plugin.EnvConfigPlugin;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;

public class MainActivity extends FlutterActivity {
    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        EnvConfigPlugin.registerWith(flutterEngine.getDartExecutor().getBinaryMessenger(),this);
        super.configureFlutterEngine(flutterEngine);
    }
}
