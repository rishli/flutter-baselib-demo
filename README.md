# 基于[flutter-baselib-plugin](https://gitee.com/rishli/flutter_baselib_plugin)库搭建的项目框架demo
<br/>


# 1、项目参数配置说明
Android工程，在android目录下的配置文件config.gradle，包含了项目工程配置参数，包括：  
* 当前使用服务器环境(如果是测试服，在应用内可以切换服务器环境，如果是正式服不能切换，正式上线打包修改为正式服)；  
* 应用版本号；  
* 是否混淆代码；  
* 控制台是否打印日志；  
在开发调试阶段可以修改这些参数；  
<br/>

如果是IOS工程（目前仅有Android端），暂时直接通过IosEnvConfig类来配置环境；  
如果有需要，可以将动态配置的参数全部写在配置文件中，包括包名和使用的sdk相关的appId和Key；

<br/>

# 2、Android调试运行说明
Android工程配置了多渠道打包，所以在debug运行时，也需要指定渠道ID，可以通过下面两种方式设置：  
方式一（推荐）：    

1、在AndroidStudio中打开工程项目，点击顶部工具栏main.dart下拉框，点击Edit Configuration；  
<font color=#ff0000>
2、在Additional run args中添加--flavor _default，点击ok；  
</font> 
3、回到工程中就可以直接点击运行安装apk到手机调试了（默认是上面设置的_default渠道）；  
<br/>

方式二：  
通过命令行运行： 
flutter run --flavor _default

<br/>

***

# 3、Android多渠道说明
 渠道ID有（13个渠道）
1. 不带渠道的包：_default  
2. 应用宝：yingyongbao  
3. 百度助手：baidu    
4. 360：_360   
5. 小米：xiaomi   
6. 华为：huawei   
7. vivo：vivo    
8. oppo：oppo   
9. UC：uc   
10. 魅族：meizu  
11. 联想：lianxiang   
12. 乐视：leshi  
13. 三星：sanxing  

<br/>

***
# 4、Android提交测试和上线说明
1、使用自己的电脑打包，打包命令（Release包）：  
* 单个渠道包：flutter build apk --flavor _default（渠道ID）  
* 一次性打所有渠道包：flutter build apk    
* 本机打release包会保存在：工程目录/local-apk/日期/时间/ 目录下；  
* 使用本机打包时，根据需求修改apk输出配置，配置参数在android/config.gradle文件中，最终生成的apk参数以config.gradle文件中的参数为准；  
* 一次性打所有渠道包，打包完成后控制台可能会显示错误info：    
  Gradle build failed to produce an .apk file. It's likely that this file was generated under xxx\build, but the tool couldn't find it.    
  实际是成功的 ，到工程目录/local-apk/日期/时间/ 目录下获取apk即可；  
<br/>

2、使用jenkins打包：参考android工程build.gradle配置  
<br/>

***
# 5、apk命名说明
运行或打包生成的apk命名如下：  
demo-v1.0.1.10-huawei-release(debug)-ConsoleShowLog(ConsoleNoLog)-Proguard(noProguard)-testServer-20210423-1543.apk  

名称-版本name.版本code-渠道-构建类型-控制台是否打印日志-是否混淆了代码-服务器环境-日期-时间.apk  

<br/>
  
 
